from pymongo import MongoClient
from pprint import pprint
import requests


def check_author_match(author_a, author_b):
    """ Verifica se author_a e author_b são a mesma pessoa.

    Returns:
        Verdadeiro se:
        1) É idêntico;
        2) O primeiro e último nome batem;
        3) Se algum possui abreviação do primeiro nome, abrevia o outro
        e compara.
    """
    if author_a == author_b:
        return True

    parts_a = author_a.split(' ')
    parts_b = author_b.split(' ')

    if parts_a[0] == parts_b[0] and parts_a[-1] == parts_b[-1]:
        return True

    # TODO: abreviação

    return False


COL_PID = 'document publishing ID (PID SciELO)'
COL_AUTHOR = 'document author'

def get_related_authors(db, base_author, start_id):
    """ Obtém autores que publicaram com author.
    Args:
        db: mongo collection
    """
    graph = []
    json_nodes = []
    json_edges = []
    new_id = start_id

    query = {
        COL_AUTHOR: base_author,
    }
    pids = db.scielo_documents_authors.find(
        query,
        {COL_PID: 1}
    )

    all_authors = []
    for pid in pids:
        pid = pid[COL_PID]

        query = {
            COL_PID: pid,
        }
        authors = db.scielo_documents_authors.find(
            query,
            {COL_AUTHOR: 1}
        )
        for item in authors:
            if item not in all_authors:
                new_id += 1
                cur_author = item[COL_AUTHOR]
                graph.append(
                    [
                        (base_author, start_id),
                        ((cur_author, new_id))
                    ]
                )
                json_nodes.append({
                    "id": new_id-1,
                    "caption": cur_author,
                    "role": "project",
                    "fun_fact": "",
                })
                json_edges.append({
                    "source": new_id-1,
                    "target": start_id,
                    "caption": ""
                })
                all_authors.append(cur_author)

    saida = {
        'authors': all_authors,
        'graph': graph,
        'jsonNodes': json_nodes,
        'jsonEdges': json_edges,
        'lastIndex': new_id
    }

    return saida

MAX_DEPTH = 1000
def find_authors_connection_scielo(author_a, author_b):
    client = MongoClient('localhost', 27017)
    db = client['sciencedots']

    json_nodes = []
    json_edges = []


    graph = []
    i = 0

    discovered = {}
    encontrou = False
    depth = 0
    s = [author_a]
    # dfs para encontrar author_b a partir do author_a
    while len(s) != 0:
        v = s.pop()
        if depth >= MAX_DEPTH:
            return

        depth += 1
        if v not in discovered:
            discovered[v] = True

            authors_out = get_related_authors(db, v, i)
            i = authors_out['lastIndex']
            authors = authors_out['authors']
            subgraph = authors_out['graph']

            for author in authors:
                if author == author_b:
                    encontrou = True
                    break

                # print(author)
                s.append(author)
                graph.append(((v, i), subgraph))
                json_nodes += authors_out['jsonNodes']
                json_edges += authors_out['jsonEdges']

            if encontrou:
                break
        # print('-----')

    # print(graph)


    new_json_nodes = []
    for i in json_nodes:
        if i not in new_json_nodes:
            new_json_nodes.append(i)

    new_json_edges = []
    for i in json_edges:
        if i not in new_json_edges:
            new_json_edges.append(i)

    # json_graph = make_graph_json(graph)
    # pprint(set(json_nodes))
    pprint(new_json_nodes)
    pprint(new_json_edges)

    json = {
        "comment": "AlchemyJS contributors",
        "nodes": new_json_nodes,
        "edges": new_json_edges,
    }

    return json
