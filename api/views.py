from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from api import related_author
import json


def find_related_author(request, author_a, author_b):
    response = related_author.find_authors_connection_scielo(
        author_a, author_b)

    resp = HttpResponse(json.dumps(response))
    resp["Access-Control-Allow-Origin"] = "*"
    resp["Access-Control-Allow-Methods"] = "GET, OPTIONS"
    resp["Access-Control-Max-Age"] = "1000"
    resp["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"


    return resp
